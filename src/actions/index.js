import * as types from './mutation-types';

export const toggleModal = () => ({
    type: types.TOGGLE_MODAL,
});

export const toggleSelectConditions = (value) => ({
    type: types.TOGGLE_SELECT_CONDITIONS,
    payload: value,
});

export const toggleSelectNotifications = (value) => ({
    type: types.TOGGLE_SELECT_NOTIFICATIONS,
    payload: value,
});

export const toggleRadio = (el) => ({
    type: types.TOGGLE_RADIO,
    payload: el,
});

export const assignTemplate = () => ({
    type: types.ASSIGN_TEMPLATE,
});