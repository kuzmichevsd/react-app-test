import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as AppActions from '../../actions';
import Button from '../Button';
import Select from '../Select';
import Radio from '../Radio';
import './index.css';

class Modal extends Component {
    render () {
        return (
            <div className={"Modal"} style={this.props.modal.open ? {display: 'block'} : {display: 'none'}}>
                <div className={"Modal-container"}>
                    <header>
                        <div>Modal window</div>
                        <div>
                            <Button title={"Assign template"} onClick={this.props.actions.assignTemplate} />
                            <Button title={"Cancel"} onClick={this.props.actions.toggleModal} />
                        </div>
                    </header>
                    <section>
                        <Select id={"conditions"} label="Conditions" setSelect={this.props.actions.toggleSelectConditions} />
                        <Select id={"notifications"} label="Notification" setSelect={this.props.actions.toggleSelectNotifications} />
                    </section>
                    <section>
                        <Radio name={"select"} id={"radio-conditions"} value={"yes"} label="Assign Conditions" checked={this.props.select.conditions === 'yes'} onChange={() => this.props.actions.toggleRadio('conditions')} />
                        <Radio name={"select"} id={"radio-notifications"} value={"yes"} label="Assign Notifiations" checked={this.props.select.notifications === 'yes'} onChange={() => this.props.actions.toggleRadio('notifications')} />
                        <Radio name={"select"} id={"radio-both"} value={"yes"} label="Assign Both" checked={this.props.select.conditions === 'yes' && this.props.select.notifications === 'yes'} onChange={() => this.props.actions.toggleRadio('both')} />
                    </section>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (store) => ({
    modal: store.modal,
    select: store.select
});

const mapDispatchToProps = (dispatch) => ({
    actions: bindActionCreators(AppActions, dispatch),
  });

export default connect(mapStateToProps, mapDispatchToProps)(Modal);