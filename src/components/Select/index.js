import React, { Component } from 'react';
import { connect } from 'react-redux';
import './index.css';

class Select extends Component {
    onChange = (e) => {
        const val = e.currentTarget.value;
        this.props.setSelect(val);
    }

    render() {
        return (
            <div className={"Select"}>
                <label>{this.props.label}</label>
                <select id={this.props.id} onChange={this.onChange} value={this.props.select[this.props.id]}>
                    <option value="yes">Yes</option>
                    <option value="no">No</option>
                </select>
            </div>
        );
    }
}

const mapStateToProps = (store) => ({
    select: store.select,
});

export default connect(mapStateToProps)(Select);