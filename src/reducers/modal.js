import { TOGGLE_MODAL } from '../actions/mutation-types';

const initialState = {
    open: false,
}

const modal = (state = initialState, action) => {
    switch (action.type) {
        case TOGGLE_MODAL:
            return {
                ...state,
                open: !state.open,
            };
        default:
            return state;
    }
}

export default modal;